added:
- preferences: optionally cricket can be played over the "whole distance" (game continues after first player won)
- X01: undo message in single dart input mode
- cricket: enhanced undo messages

fixed:
- X01 + one dart input: when input of score, was exact the same as the score left and no double or masterout was hit, but needed, the game didn't goes on
- cricket: player ranking at end of game
- crazy cricket: when undo button was clicked and player changed, the segments were selected randomly