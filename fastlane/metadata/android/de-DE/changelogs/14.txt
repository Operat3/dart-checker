hinzugefügt:
- Unterstützung für bis zu 8 Spieler (ausgenommen SET/LEG Modus)
- Hilfe: allgemeine Dartregeln

repariert:

- Hauptansicht: Auswahl der Spielernamen änderten sich, wenn die Spieleranzahl geändert wurde
- Hilfe: zwei Sprachen wurden vermischt
