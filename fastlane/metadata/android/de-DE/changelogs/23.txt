hinzugefügt:
- Elimination: unbegrenztes undo 
- Elimination: Anzeige der Punktedifferenz bis zur nächsten Eliminierung

repariert:
- Elimination: 2 Spieler: der nicht aktive Spieler wurde eliminiert in der Übersicht, egal welche Punkte der aktive Spieler geworfen hatte
- Elimination: Elimierungen wurden nicht annuliert beim Klick auf undo