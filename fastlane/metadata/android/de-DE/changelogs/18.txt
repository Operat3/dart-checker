hinzugefügt:
- Einstellungen: Aktivierund/Deaktiverung UNDO-Sicherheitsabfrage

repariert:
- SET/LEG: Layout des Hinweises nach Beendigung eines Legs
- X01: Layout der Vorschläge
- X01: Layout des aktiven Spielernamens