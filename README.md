Dart Checker
------------

An app for Android, that assists players of darts in counting their scores.


Dart Checker takes care of your score, while you are playing darts
alone or with friends. Its the better form of a scoreboard. No manual calculating
is needed anymore. Just put in your hits, like you would tell.

    Features

    * games: single X01 (301, 501), SET/LEG, FREE training mode (variable score), (CRAZY) CRICKET, ELIMINATION, HALVE IT
    * 1-8 player (except SET/LEG mode)
    * master, double and single out
    * checkout suggestions
    * multiple undo in a row of mistouched inputs
    * statistics (about as many matches and player as you like)
    * two input variants (dart by dart or total score of three darts)
    * power saving dark theme or light one
    * language: german / english / spanish / french
    * offline - absolutely no internet connection is needed or used, beside app download ;-)



[<img src="https://f-droid.org/badge/get-it-on.png"
     alt="Get it on F-Droid"
     height="80">](https://f-droid.org/packages/com.DartChecker/)
